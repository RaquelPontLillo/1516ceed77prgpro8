package org.ceedcv.ceed77prgpro8.vista;

import java.awt.Color;
import javax.swing.*;

public class VistaGrafica extends JFrame {
    //Panel
    JDesktopPane panel = new JDesktopPane();
    
    //Logo
    JLabel logo = new JLabel();
    
    //Menú
    JMenuBar menuSuperior = new JMenuBar();
    JMenu menuAlumnos = new JMenu();    
    JMenu menuCursos = new JMenu();
    JMenu menuMatriculas = new JMenu();
    JMenu menuBbdd = new JMenu();
    JMenu menuDocumentacion = new JMenu();
    JMenu menuAcerca = new JMenu();
    JMenu menuSalir = new JMenu();
    
    //Items del menu
    JMenuItem itemAlumnos = new JMenuItem();
    JMenuItem itemCursos = new JMenuItem();
    JMenuItem itemMatriculas = new JMenuItem();
    JMenuItem itemCrearbbdd = new JMenuItem();
    JMenuItem itemCreartablas = new JMenuItem();
    JMenuItem itemCreardatos = new JMenuItem();
    JMenuItem itemDocWeb = new JMenuItem();
    JMenuItem itemDocPdf = new JMenuItem();
    JMenuItem itemAcerca = new JMenuItem();
    JMenuItem itemSalir = new JMenuItem();


    public VistaGrafica() {
        //Llamamos al constructor de la clase JFrame
        super("Menú principal");
        
        //Configuramos el tamaño, comportamiento del aspa y aspecto general de la GUI
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setJMenuBar(menuSuperior);
        menuSuperior.add(menuAlumnos);
        menuSuperior.add(menuCursos);
        menuSuperior.add(menuMatriculas);
        menuSuperior.add(menuBbdd);
        menuSuperior.add(menuDocumentacion);
        menuSuperior.add(menuAcerca);
        menuSuperior.add(menuSalir);
        menuAlumnos.add(itemAlumnos);
        menuCursos.add(itemCursos);
        menuMatriculas.add(itemMatriculas);
        menuBbdd.add(itemCrearbbdd);
        menuBbdd.add(itemCreartablas);
        menuBbdd.add(itemCreardatos);
        menuDocumentacion.add(itemDocWeb);
        menuDocumentacion.add(itemDocPdf);
        menuAcerca.add(itemAcerca);
        menuSalir.add(itemSalir);
        
        
        logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo_mayus.png")));
        
        menuAlumnos.setText("Alumnos");
        menuAlumnos.setIcon(new ImageIcon(getClass().getResource("/imagenes/alumno.png")));
        menuCursos.setText("Cursos");
        menuCursos.setIcon(new ImageIcon(getClass().getResource("/imagenes/curso.png")));
        menuMatriculas.setText("Matriculas");
        menuMatriculas.setIcon(new ImageIcon(getClass().getResource("/imagenes/matricula.png")));
        menuBbdd.setText("Base de datos");
        menuBbdd.setIcon(new ImageIcon(getClass().getResource("/imagenes/bbdd.png")));
        menuDocumentacion.setText("Documentación");
        menuDocumentacion.setIcon(new ImageIcon(getClass().getResource("/imagenes/documentacion.png")));
        menuAcerca.setText("Acerca");
        menuAcerca.setIcon(new ImageIcon(getClass().getResource("/imagenes/acerca.png")));
        menuSalir.setText("Salir");
        menuSalir.setIcon(new ImageIcon(getClass().getResource("/imagenes/cerrar.png")));
        
        itemAlumnos.setText("Gestionar alumnos");
        itemAlumnos.setIcon(new ImageIcon(getClass().getResource("/imagenes/gestionar.png")));
        itemCursos.setText("Gestionar cursos");
        itemCursos.setIcon(new ImageIcon(getClass().getResource("/imagenes/gestionar.png")));
        itemMatriculas.setText("Gestionar matrículas");
        itemMatriculas.setIcon(new ImageIcon(getClass().getResource("/imagenes/gestionar.png")));
        itemCrearbbdd.setText("Crear base de datos");
        itemCrearbbdd.setIcon(new ImageIcon(getClass().getResource("/imagenes/basedatos.png")));
        itemCreartablas.setText("Crear tablas");
        itemCreartablas.setIcon(new ImageIcon(getClass().getResource("/imagenes/tablas.png")));
        itemCreardatos.setText("Crear datos de ejemplo");
        itemCreardatos.setIcon(new ImageIcon(getClass().getResource("/imagenes/datos.png")));
        itemDocWeb.setText("Abrir vía web");
        itemDocWeb.setIcon(new ImageIcon(getClass().getResource("/imagenes/web.png")));
        itemDocPdf.setText("Abrir en PDF");
        itemDocPdf.setIcon(new ImageIcon(getClass().getResource("/imagenes/pdf.png")));
        itemAcerca.setText("Acerca de Matrículas App.");
        itemSalir.setText("Salir de la aplicación");
        
        panel.setOpaque(true);
        panel.setBackground(Color.lightGray);
        
        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(logo, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(logo, javax.swing.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
        );
        panel.setLayer(logo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel)
        );
        
//        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
//        panel.setLayout(panelLayout);
//        panelLayout.setHorizontalGroup(
//            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 750, Short.MAX_VALUE)
//        );
//        panelLayout.setVerticalGroup(
//            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addGap(0, 480, Short.MAX_VALUE)
//        );
//        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
//        getContentPane().setLayout(layout);
//        layout.setHorizontalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
//        );
//        layout.setVerticalGroup(
//            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//            .addComponent(panel)
//        );

        pack();
        
        //Hacemos visible la GUI
        setVisible(true);
    }
    
    //Getters
    public JMenuItem getAlumno() {
        return itemAlumnos;
    }
    
    public JMenuItem getCurso() {
        return itemCursos;
    }
    
    public JMenuItem getMatricula() {
        return itemMatriculas;
    }
    
    public JMenuItem getCrearbbdd() {
        return itemCrearbbdd;
    }
    
    public JMenuItem getCreartablas() {
        return itemCreartablas;
    }
    
    public JMenuItem getCreardatos() {
        return itemCreardatos;
    }
    
    public JMenuItem getDocWeb() {
        return itemDocWeb;
    }
    
    public JMenuItem getDocPdf() {
        return itemDocPdf;
    }
    
    public JMenuItem getAcerca() {
        return itemAcerca;
    }
    
    public JMenuItem getSalir() {
        return itemSalir;
    }
    
    public JDesktopPane getEscritorio() {
        return panel;
    }
}