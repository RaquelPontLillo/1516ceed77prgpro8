package org.ceedcv.ceed77prgpro8.controlador;

import org.ceedcv.ceed77prgpro8.modelo.IModelo;
import org.ceedcv.ceed77prgpro8.vista.VistaGrafica;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Main {
    public static void main (String[] args) { 
        IModelo modelo = null;
        VistaGrafica vista = null;
        Controlador controlador = new Controlador (modelo, vista);
    }
}