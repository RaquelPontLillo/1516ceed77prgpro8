package org.ceedcv.ceed77prgpro8.controlador;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.ceedcv.ceed77prgpro8.modelo.IModelo;
import org.ceedcv.ceed77prgpro8.modelo.ModeloMysql;
import org.ceedcv.ceed77prgpro8.modelo.Usuario;
import org.ceedcv.ceed77prgpro8.vista.Funciones;
import org.ceedcv.ceed77prgpro8.vista.VistaGrafica;
import org.ceedcv.ceed77prgpro8.vista.VistaGraficaAcerca;
import org.ceedcv.ceed77prgpro8.vista.VistaGraficaAlumno;
import org.ceedcv.ceed77prgpro8.vista.VistaGraficaCurso;
import org.ceedcv.ceed77prgpro8.vista.VistaGraficaMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Controlador implements ActionListener {
    IModelo modelo = null;
    VistaGrafica vista = null;
    private Funciones f = new Funciones();
    
    public Controlador (IModelo m, VistaGrafica v) {
        boolean valido = false;
        
        //Validamos el acceso
        while (!valido) {
            valido = validarAcceso();
            if (!valido) {
                f.error("El usuario o la contraseña no coinciden o son erróneos.");
            }
        }
        
        //Asignamos el modelo y la vista
        vista = new VistaGrafica();
        modelo = m;
        
        //AÃ±adimos los listeners
        vista.getAlumno().addActionListener(this);
        vista.getCurso().addActionListener(this);
        vista.getMatricula().addActionListener(this);
        vista.getCrearbbdd().addActionListener(this);
        vista.getCreartablas().addActionListener(this);
        vista.getCreardatos().addActionListener(this);
        vista.getDocWeb().addActionListener(this);
        vista.getDocPdf().addActionListener(this);
        vista.getAcerca().addActionListener(this);
        vista.getSalir().addActionListener(this);
  }
    
    //Implementamos los mÃ©todos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getAlumno()) {
            try {
                alumno();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getCurso()) {
            try {
                curso();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getMatricula()) {
            try {
                matricula();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (objeto == vista.getCrearbbdd()) {
            crearBd();
        } else if (objeto == vista.getCreartablas()) {
            crearTablas();
        } else if (objeto == vista.getCreardatos()) {
            crearDatos();
        } else if (objeto == vista.getDocPdf()) {
            documentacionPdf();
        } else if (objeto == vista.getDocPdf()) {
            documentacionPdf();
        } else if (objeto == vista.getDocWeb()) {
            documentacionWeb();
        } else if (objeto == vista.getAcerca()) {
            acerca();
        }
    }
    
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    public void mouseClicked(MouseEvent e) {
        //No hacer nada
    }
    
    private void salir() {
        System.exit(0);
    }
    
    private void alumno() throws IOException {
        //modelo = new ModeloFichero();
        modelo = new ModeloMysql();
        VistaGraficaAlumno v = VistaGraficaAlumno.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ControladorAlumno ca = new ControladorAlumno(modelo, v);
    }
    
    private void curso() throws IOException, PropertyVetoException {
        //modelo = new ModeloFichero();
        modelo = new ModeloMysql();
        VistaGraficaCurso v = VistaGraficaCurso.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        ControladorCurso cc = new ControladorCurso(modelo, v);    
    }
    
    private void matricula() throws IOException {
        //modelo = new ModeloFichero();
        modelo = new ModeloMysql();
        VistaGraficaMatricula v = VistaGraficaMatricula.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        ControladorMatricula cm = new ControladorMatricula(modelo, v);
    }
    
    private void documentacionWeb() {
        String url = "https://docs.google.com/document/d/1pTpLNawKELyIV_Vn_pu5DMgM-HdIpmN13lkvVuf6X3o/edit?ts=5624d784";
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void documentacionPdf() {
        try {
            String ruta = "src/main/java/org/ceedcv/ceed77prgpro8/documentacion/documentacion.pdf";
            File pdf = new File(ruta);
            Desktop.getDesktop().open(pdf);
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void crearBd() {
        ModeloMysql m = new ModeloMysql();
        String s = m.crearBd();
         if (s != null) {
             f.error("Ha ocurrido un error durante la instalación \n "
                   + "Error: " + s);
         }
    }
    
    private void crearTablas() {
        ModeloMysql m = new ModeloMysql();
        String s = m.crearTablas();
         if (s != null) {
             f.error("Ha ocurrido un error durante la creación de tablas \n "
                   + "Error: " + s);
        }
    }
    
    private void crearDatos() {
        ModeloMysql m = new ModeloMysql();
        String s = m.crearDatos();
         if (s != null) {
             f.error("Ha ocurrido un error durante la creación de datos \n "
                   + "Error: " + s);
        }
    }
    
    private void acerca() {
        VistaGraficaAcerca v = VistaGraficaAcerca.getInstance();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    private boolean validarAcceso() {
        Usuario u = new Usuario();
        String user = JOptionPane.showInputDialog(null, "Introduce el usuario (root)", "root");
        String pass = JOptionPane.showInputDialog(null, "Introduce la contraseña (pass)", "pass");
        return user.equals(u.getUsuario()) && pass.equals(u.getPassword());
    }
}
